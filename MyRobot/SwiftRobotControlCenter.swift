//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L555H" //  Level name
        super.viewDidLoad()
    }
    
    var l = 1 // count of lines
    var c = 4 // count of candy
    var candy = 0 //
    var candyInAString = 0 //
    var numberString = 0 // number line
    
    override func run() {
        
        for _ in 0..<l {
            for _ in 0..<c {
                putAndGo()
            }
            nextLineJump()
        }
    }
    
    // start block functions
    
    func putAndGo() {
        put()
        if frontIsClear {
            move()
        }
    }
    
    func nextLineJump() {
        if facingRight {
            turnRight()
            jump()
            turnRight()
        }
        else if facingLeft{
            turnLeft()
            jump()
            turnLeft()
        }
    }
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
    
    func jump(){
        if facingDown, frontIsClear {
            move()
        }
        if facingDown, frontIsClear {
            move()
        }
    }
    
    // end functions
    
} // End controller

// test commit git hub ))) - 

